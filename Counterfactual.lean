namespace Counterfactual
inductive Conditional where
  | proposition: String -> Conditional
  | would: Conditional -> Conditional -> Conditional
  | might: Conditional -> Conditional -> Conditional
  | not: Conditional -> Conditional

def toRepr : Conditional -> Std.Format
  | Conditional.would left right =>  "(" ++ toRepr left ++ Std.Format.text " ▢→ "  ++ toRepr right ++ ")"
  | Conditional.might left right =>  "(" ++ toRepr left ++ Std.Format.text " ◇→ "  ++ toRepr right ++ ")"
  | Conditional.proposition name => name
  | Conditional.not op => "~" ++ toRepr op

instance : Repr Conditional where
  reprPrec cond _ := toRepr cond

infixl:65   " ||-> " => Conditional.would
infixl:65   " <>-> " => Conditional.might
infixl:65   " ▢→ " => Conditional.would
infixl:65   " ◇→ " => Conditional.might
prefix:66   " ~ " => Conditional.not

axiom mightToWould (ϕ ψ : Conditional) : ϕ ◇→ ψ = ~(ϕ ▢→ ~ψ)
axiom wouldToMight (ϕ ψ : Conditional) : ϕ ▢→ ψ = ~(ϕ ◇→ ~ψ)

end Counterfactual
