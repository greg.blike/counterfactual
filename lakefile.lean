import Lake
open Lake DSL

package «counterfactual» {
  -- add package configuration options here
}

lean_lib «Counterfactual» {
  -- add library configuration options here
}

@[default_target]
lean_exe «counterfactual» {
  root := `Main
}
