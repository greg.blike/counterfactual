import «Counterfactual»

open Counterfactual

def concept1 := Conditional.proposition "concept1"
def concept2 := Conditional.proposition "concept2"

def impl := ~ concept1 ◇→ concept2

def main : IO Unit :=
  IO.println $ repr impl
